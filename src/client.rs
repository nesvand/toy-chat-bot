pub mod slack {
    const SLACK_API_TOKEN_ENV_VAR: &str = "SLACK_API_TOKEN";
    const SLACK_API_URL_ENV_VAR: &str = "SLACK_API_URL";
    const TEXT_KEY: &str = "text";
    const CHANNEL_KEY: &str = "channel";
    const BOT_PROFILE_KEY: &str = "bot_profile";

    use std::env;
    use bytes::buf::BufExt as _;
    use hyper::{header, Client, Body, Method, Request, Response, StatusCode};
    use hyper::client::HttpConnector;
    use hyper_tls::HttpsConnector;
    use serde_json::{Map, Value};

    pub type GenericError = Box<dyn std::error::Error + Send + Sync>;
    pub type Result<T> = std::result::Result<T, GenericError>;
    pub type HttpsClient = Client<HttpsConnector<HttpConnector>>;

    lazy_static! {
        static ref SLACK_API_TOKEN: String = env::var(SLACK_API_TOKEN_ENV_VAR).unwrap();
        static ref SLACK_API_URL: String = env::var(SLACK_API_URL_ENV_VAR)
            .unwrap_or_else(|_| String::from("https://api.slack.com/api"));
    }

    #[derive(Deserialize, Debug)]
    struct SlackEvent {
        #[serde(rename = "type")]
        event_type: String,
        event_ts: String,
        #[serde(flatten)]
        extra: Map<String, Value>,
    }

    #[derive(Deserialize, Debug)]
    #[serde(tag = "type", rename_all = "snake_case")]
    enum SlackEventMessage {
        UrlVerification {
            token: String,
            challenge: String,
        },
        EventCallback {
            token: String,
            team_id: String,
            api_app_id: String,
            event: SlackEvent,
            event_id: String,
            event_time: i32,
            authed_users: Vec<String>,
        },
    }

    #[derive(Serialize, Debug)]
    struct SlackChatPostMessage {
        channel: String,
        text: String,
    }

    pub async fn service_fn(req: Request<Body>, client: HttpsClient) -> Result<Response<Body>> {
        match (req.method(), req.uri().path()) {
            (&Method::POST, "/events") => {
                let whole_body = hyper::body::aggregate(req.into_body()).await?;
                let event_message = serde_json::from_reader(whole_body.reader()).unwrap();

                match event_message {
                    SlackEventMessage::UrlVerification { challenge, .. } => {
                        Ok(Response::new(Body::from(challenge)))
                    }
                    SlackEventMessage::EventCallback { event, .. } => {
                        // If it's a bot, we ignore the event
                        if event.extra.contains_key(BOT_PROFILE_KEY) {
                            println!("No need to listen to another bot: {:#?}", event);

                            return Ok(Response::new(Body::empty()));
                        }

                        if let Some(text) = event.extra.get(TEXT_KEY)
                            .and_then(|t| t.as_str())
                            .and_then(|t| if t.contains("echo") { Some(t) } else { None })
                            .or_else(|| None) {
                            let channel = event.extra.get(CHANNEL_KEY)
                                .unwrap().as_str().unwrap();
                            let chat_post_message = serde_json::to_string(&SlackChatPostMessage {
                                channel: String::from(channel),
                                text: String::from(text),
                            });
                            let req = Request::builder()
                                .method(Method::POST)
                                .uri(format!("{}/chat.postMessage", SLACK_API_URL.to_string()))
                                .header(header::CONTENT_TYPE, "application/json")
                                .header(header::AUTHORIZATION, format!("Bearer {}", SLACK_API_TOKEN.to_string()))
                                .body(Body::from(chat_post_message.unwrap()))
                                .unwrap();
                            let resp = client.request(req).await?;

                            println!("Echo Returned: {}", resp.status());
                        }

                        println!("{:#?}", event);

                        Ok(Response::new(Body::empty()))
                    }
                }
            }
            _ => {
                Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(Body::empty())
                    .unwrap())
            }
        }
    }
}