#[macro_use]
extern crate serde;
#[macro_use]
extern crate lazy_static;

pub mod client;

use client::slack;

use hyper::service::{make_service_fn, service_fn};
use hyper::{Client, Server};
use hyper_tls::HttpsConnector;

#[tokio::main]
async fn main() -> slack::Result<()> {
    let https = HttpsConnector::new();
    let client = Client::builder()
        .build::<_, hyper::Body>(https);
    let svc = make_service_fn(
        move |_| {
            let c = client.clone();

            async {
                Ok::<_, hyper::Error>(service_fn(move |req| {
                    slack::service_fn(req, c.to_owned())
                }))
            }
        }
    );

    let addr = ([127, 0, 0, 1], 3000).into();
    let server = Server::bind(&addr).serve(svc);

    println!("Listening on http://{}", addr);

    server.await?;

    Ok(())
}
